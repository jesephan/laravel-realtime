<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Events\MessagePosted;
use App\Message;

Route::get('/', function () {
    return view('chat-home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/get-messages', function(){
	return request()->json(200, Message::with('user')->get());
});

Route::post('/send-message', function(){
	$msg = request()->user()->messages()
		->create(['message' => request()->params['message']]);

	broadcast(new MessagePosted($msg, request()->user()))->toOthers();
	
	return request()->json(['success']);
});
