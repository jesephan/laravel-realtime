<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div id="app">
            <div class="container">
                @if (Auth::check())
                    <h1 id="current-user">{{Auth::user()->name}}</h1>
                @else
                    <h1>Not logged in</h1>
                @endif
            </div>
            
            <chat-messages v-bind:messages="messages"></chat-messages>
            <chat-form v-on:message-sent="updateMessages"></chat-form>
        </div>

        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>
