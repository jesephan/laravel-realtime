
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('message', require('./components/message.vue'));
Vue.component('example', require('./components/Example.vue'));
Vue.component('chat-messages', require('./components/chat-messages.vue'));
Vue.component('chat-form', require('./components/chat-form.vue'));

const app = new Vue({
    el: '#app',
    mounted() {
    	var self = this;
    	axios.get('/get-messages')
    	.then((response) => {
    		self.messages = response.data;
    	});

        Echo.join('chatroom')
            .listen('MessagePosted', (e) => {
                self.messages.push({
                    user: e.user,
                    message: e.msg.message
                })
            });
    },
    data: {
    	messages: []
    },
    methods: {
    	updateMessages(user) {
            console.log(user);
    		this.messages.push({
                user: {name: user.user},
                message: user.message
            });
    	}
    }
});
